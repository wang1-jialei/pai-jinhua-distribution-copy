import Vue from 'vue'
import App from './App'
import util from './common/util.js' //通用方法
Vue.prototype.$util_diy = util ; 
//全局变量
Vue.prototype.globalData={
	userInfo:null
};
// #ifdef H5
import wx from './common/wechat.js';//微信jssdk
Vue.prototype.$wx = wx;
// #endif
//toast提示框，带遮挡层
Vue.prototype.$toast=function(msg,icon,durationTime){
	uni.showToast({
		icon:typeof(icon)=='string'?icon:'none',
		duration:durationTime>0?durationTime:1500,
		mask:true,//防止点击穿透
		title:msg
	});
}
//loading提示框，带遮挡层，建议用于避免表单重复提交
Vue.prototype.$showLoading=function(msg){
	uni.showLoading({ 
		mask:true,//防止点击穿透
		title:msg
	});
}

//loading提示框 - 关闭
Vue.prototype.$hideLoading=function(msg){
	uni.hideLoading();
}

// 请求接口封装
import api from './static/js/api.js';
Vue.prototype.$api = api;

//图片接口封装
import Util from './static/js/util.js';
Vue.prototype.$util = Util;
// 请求配置接口封装
import config from './config.js';
Vue.prototype.$config = config;

//常用函数封装
import func from './static/js/function.js';
Vue.prototype.$func = func;

//md5加密
import md5 from './static/js/md5.js';
Vue.prototype.$md5 = md5;



/** ColorUI   START **/
// colorui 设置默认顶部导航高度
Vue.prototype.CustomBar = 60; 
// 自定义头部
import cuCustom from '@/components/colorui/components/cu-custom.vue'
Vue.component('cu-custom', cuCustom)
/** ColorUI   END   **/

/** uViewUI   START  **/
// 引入uView
import uView from "components/uview-ui";
Vue.use(uView);
// 引入uView对小程序分享的mixin封装
let mpShare = require('./components/uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare)

/** uViewUI   END   **/

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
