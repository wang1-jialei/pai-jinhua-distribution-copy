export default {
	methods:{
		//添加的方法选择
		select(type,text = '',index = 0){
			console.log(type,text)
			if(type == 'openurl'){
				// #ifdef H5
				location.href = text;
				// #endif
				// #ifdef APP
				plus.runtime.openURL(text); 
				// #endif
			}
			switch(JSON.stringify(this.getdata.btstatus)){
				case '0':
					uni.showToast({title:'请点击报名',icon:'none'})
					break;
				case '1':
					this[type](text,index);
					break;
				case '2':
					uni.showToast({title:'审核中',icon:'none'})
					break;
				case '3':
					uni.showToast({title:'已结束',icon:'none'})
					break;
				case '4':
					uni.showToast({title:'未开始',icon:'none'})
					break;
			}
		},
		//添加成功图片
		addimg(data,index){
			console.log("this.$",this.$util_diy)
			this.$util_diy.selectImage({
				num:1,
				success:res=>{
					if(res.judge) {
						let data = JSON.parse(res.data),url = data.result.url;
						this.setImage = this.setImage.concat(url);
						if(this.getdata.making_context[index].addimg){
							this.getdata.making_context[index].addimg = this.getdata.making_context[index].addimg.concat(url);
						}else{
							this.getdata.making_context[index].addimg = [url]
						}
						this.$forceUpdate();
					}
				}
			})
		},
		//保存扫码
		preservation(data){
			// #ifdef H5
			this.$util_diy.getImglocal(data)
			// #endif
			// #ifdef APP
			let that = this;
			uni.showModal({
				content: '是否保存图片到相册',
				success: (res) => {
					uni.hideLoading();
					if (res.confirm) {
						// 向用户发起授权请求
						that.downLoadImg(data);
					}
				}
			});
			// #endif
		},
		//打开链接
		openurl(data){
			// #ifdef H5
			location.href = data;
			// #endif
			// #ifdef APP
			plus.runtime.openURL(res.data.pay); 
			// #endif
		},
		//复制链接
		copyurl(data){
			this.$util_diy.copy(data);
		},
		//保存到相册
		downLoadImg(data) {
			uni.showLoading({
				title: '加载中'
			});
			uni.downloadFile({
				url: this.$util_diy.img(data),
				success: (res) => {
					uni.hideLoading();
					if (res.statusCode === 200) {
						uni.saveImageToPhotosAlbum({
							filePath: res.tempFilePath,
							success: function(res) {
								uni.showToast({
									title: "保存成功",
									icon: "none"
								});
							},
							fail: function(err) {
								console.log('数据',err)
								uni.showToast({
									title: "保存失败，请稍后重试",
									icon: "none"
								});
							}
						});
					}
				},
				fail: (err) => {
					uni.showToast({
						title: "失败啦",
						icon: "none"
					});
				}
			})
		},
	}
}