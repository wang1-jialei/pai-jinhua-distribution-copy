import Config from '@/config.js';  
import Request from '@/components/luch-request/index.js'
import util from '@/static/js/util.js'
var apis={};
apis.request=function(options) {  
    var url0 = "", that = this, now = Date.parse(new Date()) / 1000; 
    if (typeof (options) != "object") {
		url0 = options;
		options={};
    } else {
		url0 =Config.url + (options.url)+"?"+new Date().getTime();
		if (options.url.indexOf("http") != -1) {//自定义url
			url0 = options.url;
		}
    }
	var header = {};
	if(typeof(options.header)!="undefined"){
	  header=options.header;
	}
	let cookie = uni.getStorageSync('cookie');
	if (cookie) {
	  header.Cookie =  cookie ;
	}  
	const http = new Request(); 
	if(options.data!=undefined){
	} 
    if(options.method!=undefined&&options.method.toUpperCase()=="GET"){
	    http.get(url0, {
		   params:options.data,
		   header:header,
		   // //其他参数
		}).then(res => { 
		   if(res.data.msg == 'TOKEN错误' || res.data.msg == '用户不存在') {
				util.redirectTo('/pages/login/login', {}, 'redirectTo')
			}
			
			if (res && res.cookies&&res.cookies.length>0) {
				uni.setStorageSync('cookie', res.cookies[0]);   //保存Cookie到Storage
			} 
			
			if (options.success!=undefined&&typeof (options.success) == "function") {
				options.success(res.data);//回调
			}
	    }).catch(err => { 
			if (options.fail!=undefined&&typeof (options.fail) == "function") { 
				options.fail(err);//回调
			}
	    })
    }else{
	   http.post(url0,options.data, {
	       //其他参数
		    header:header,
	   }).then(res => { 
		   if(res.data.msg == 'TOKEN错误' || res.data.msg == '用户不存在') {
			   util.redirectTo('/pages/login/login', {}, 'redirectTo')
		   }
			if (res && res.cookies&&res.cookies.length>0) {
				uni.setStorageSync('cookie', res.cookies[0]);   //保存Cookie到Storage
			} 
			if (options.success!=undefined&&typeof (options.success) == "function") {
				options.success(res.data);//回调
			}
	   }).catch(err => {
			if (options.fail!=undefined&&typeof (options.fail) == "function") { 
				options.fail(err);//回调
			}
	   })
   }
 }
apis.upload=function(options){ 
	var url0 = "", that = this, now = Date.parse(new Date()) / 1000;
	if (typeof (options) != "object") {
		url0 = options;
		options={};
	} else {
		url0 =Config.url + (options.url);
		if (options.url.indexOf("http") != -1) {//自定义url
			url0 = options.url;
		}
	} 
	const http = new Request();
	 http.upload(url0, {
	    params: {}, /* 会加在url上 */
	    // #ifdef APP-PLUS || H5
	    files: [], // 需要上传的文件列表。使用 files 时，filePath 和 name 不生效。App、H5（ 2.6.15+）
	    // #endif
	    // #ifdef MP-ALIPAY
	   // fileType: 'image/video/audio', // 仅支付宝小程序，且必填。
	    // #endif
	    filePath: options.path, // 要上传文件资源的路径。
	    // 注：如果局部custom与全局custom有同名属性，则后面的属性会覆盖前面的属性，相当于Object.assign(全局，局部)
	    custom: {auth: true}, // 可以加一些自定义参数，在拦截器等地方使用。比如这里我加了一个auth，可在拦截器里拿到，如果true就传token
	    name: options.name?options.name:"file", // 文件对应的 key , 开发者在服务器端通过这个 key 可以获取到文件二进制内容
	    header: {},  /* 会与全局header合并，如有同名属性，局部覆盖全局 */
	    formData: options.data?options.data:{}, // HTTP 请求中其他额外的 form data
	    // 返回当前请求的task, options。请勿在此处修改options。非必填
	    getTask: (task, options) => {
	      // task.onProgressUpdate((res) => {
	      //   console.log('上传进度' + res.progress);
	      //   console.log('已经上传的数据长度' + res.totalBytesSent);
	      //   console.log('预期需要上传的数据总长度' + res.totalBytesExpectedToSend);
	      //
	      //   // 测试条件，取消上传任务。
	      //   if (res.progress > 50) {
	      //     uploadTask.abort();
	      //   }
	      // });
	    }
	 }).then(res => {
	 	if (options.success!=undefined&&typeof (options.success) == "function") {
	 		options.success(res.data);//回调
	 	}
	 }).catch(err => {
	 	if (options.fail!=undefined&&typeof (options.fail) == "function") { 
	 		options.fail(err);//回调
	 	}
	 })
}
module.exports = apis;
