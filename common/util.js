import http from '../config.js'
/**
 * 公共方法
 */
export default {
	/**
	 * 页面跳转
	 * @param {string} to 跳转链接 /pages/index/index
	 * @param {Object} param 参数 {key : value, ...}
	 * @param {string} mode 模式 
	 */
	redirectTo(url, param, mode) {
		//拼接页面间互传的数据
		if (param != undefined) {
			Object.keys(param).forEach((item, index) => {
				if (index == 0) {
					url += '?' + item + '=' + param[item];
				} else {
					url += '&' + item + '=' + param[item];
				}
			})
		}
		switch (mode) {
			//关闭当前页面，跳转
			case 'redirectTo':
				uni.redirectTo({
					url: url
				});
				break;
				//关闭所有页面跳转
			case 'reLaunch':
				uni.reLaunch({
					url: url
				});
				break;
				//导航栏页面跳转
			case 'switchTab':
				uni.switchTab({
					url: url
				});
				break;
				// 跳转不清除路由缓存
			default:
				uni.navigateTo({
					url: url
				});
		}
	},
	/**
	 * 选择图片
	 */
	selectfile(data = {}) {
		// #ifdef H5
		uni.chooseFile({
			success: (res) => {
				res.tempFilePaths.forEach((item, index) => {
					this.getFile(item, (res) => {
						if (typeof data.success == 'function') data.success(res)
					})
				})
			},
		})
		// #endif

	},
	/**
	 * @上传文件
	 */
	getFile(item, callback) {
		uni.uploadFile({
			url: http.url + '/common/uploadFile',
			filePath: item,
			name: 'file',
			success: (res) => {
				if (typeof callback == 'function') callback(res)
			},
		});
	},
	/**
	 * 选择图片
	 * @param {string}  //num获取的一些参数
	 * @param {function} //callback获取数据之后的回调
	 * @param {b} //judge判断是否等待中
	 */
	selectImage(data = {}) {
		uni.chooseImage({
			count: data.num,
			sourceType: ['album'],
			success: (res) => {
				if (typeof data.success == 'function') {
					data.success({
						judge: false,
						data: res.tempFilePaths
					})
				}
				uni.uploadFile({
					url: http.url + '/api/Upload/upload', //仅为示例，非真实的接口地址
					filePath: res.tempFilePaths[0],
					name: 'file',
					formData: {
						'user': 'test'
					},
					success: (uploadFileRes) => {
						if (typeof data.success == 'function') data.success({
							judge: true, //加载样子判断
							data: uploadFileRes.data
						});
					}
				});
			},
		})
	},
	/**
	 * 复制
	 * @param {Object} callback
	 */
	copy(value, callback) {
		// #ifdef H5
		var oInput = document.createElement('textarea'); //创建一个隐藏input（重要！）
		oInput.value = value; //赋值
		oInput.setAttribute("readonly", "readonly");
		document.body.appendChild(oInput);
		oInput.select(); // 选择对象
		document.execCommand("Copy"); // 执行浏览器复制命令
		oInput.className = 'oInput';
		oInput.style.display = 'none';
		uni.hideKeyboard();
		uni.showToast({
			title: '复制成功',
			icon: 'none'
		});
		typeof callback == 'function' && callback();
		// #endif

		//#ifndef H5
		uni.setClipboardData({
			data: value,
			success: () => {
				typeof callback == 'function' && callback();
			}
		});
		// #endif
	},
	/**
	 * 拼接图片
	 * @param {Object} url 链接
	 * @param {Object} type 判断拼接类型
	 */
	img(url, type, imgtype) {
		// console.log('url,type,imgtype',url,type,imgtype);
		switch (type) {
			case 'invitation':
				return http.url + url;
				break;
			case 'html':
				return http.url + 'uploads' + url + imgtype;
				break;
			default:
				if (url && url.indexOf(http.baseUrl) == -1) {
					let imgsUrl = url.split('uploads')
					if (imgsUrl.length == 1) {
						if (imgsUrl.indexOf('static/img') != -1) return url
						return http.url + '/uploads' + imgsUrl[0];
					}
					return http.url + '/uploads' + imgsUrl[1];
				}
				return url;
		}

	},
	/**
	 * 时间戳转时间
	 */
	timestampToTime(timestamp) {
		var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
		var Y = date.getFullYear() + '-';
		var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
		var D = date.getDate() + ' ';
		var h = date.getHours() + ':';
		var m = date.getMinutes() + ':';
		if (m.split('').length == 2) m = '0' + date.getMinutes() + ':';
		var s = date.getSeconds();
		if (s.toString().split('').length == 1) s = '0' + s;
		return Y + M + D + h + m + s;
	},
	/**
	 * 拼接html图片地址
	 */
	SplicingImgSrc(content) {
		let arr = JSON.parse(JSON.stringify(content.split('../uploads')));
		arr.forEach((item, index) => {
			if (item.indexOf('jpg') != -1) {
				arr[index] = item.split('jpg');
				arr[index][0] = this.img(arr[index][0], 'html', 'jpg');
				arr[index] = arr[index].join('')
			}
			if (item.indexOf('png') != -1) {
				arr[index] = item.split('png');
				arr[index][0] = this.img(arr[index][0], 'html', 'png');
				arr[index] = arr[index].join('')
			}
		})
		return arr.join('');
	},
	/**
	 * 保存图片到本地
	 * @param {Object} url 图片链接
	 */
	getImglocal(url) {
		// #ifdef APP
		// 先下载图片
		uni.downloadFile({
			url: url,
			success: (res) => {
				console.log(res);
				// 获取到图片本地地址后再保存图片到相册（因为此方法不支持远程地址）
				uni.saveImageToPhotosAlbum({
					filePath: res.tempFilePath,
					success: () => {
						uni.showToast({
							title: "保存成功！",
							icon: 'none'
						});
					},
					fail: (err) => {
						uni.showToast({
							title: "保存失败",
							icon: 'none'
						});
					},
				});
			},
		});
		// #endif
		// #ifdef H5
		var oA = document.createElement("a");
		oA.download = ''; // 设置下载的文件名，默认是'下载'
		oA.href = url;
		document.body.appendChild(oA);
		oA.click();
		oA.remove(); // 下载之后把创建的元素删除
		// #endif
	},
	/**
	 * 分享到微信
	 */
	share(src) {
		// #ifdef H5
		// #endif

		// #ifdef APP
		uni.share({
			provider: "weixin",
			scene: "WXSceneSession",
			type: 2,
			imageUrl: src,
			success: function(res) {
				console.log("success:" + JSON.stringify(res));
			},
			fail: function(err) {
				console.log("fail:" + JSON.stringify(err));
			}
		})
		// #endif
	},
	//文件预览
	prefile(e,callback) {
		let that = this;
		let url = http.url + '/uploads' + e;
		uni.showLoading({title: '加载中'});
		uni.downloadFile({
			url: url,
			success: function(res) {
				uni.hideLoading();
				let filepathss = plus.io.convertLocalFileSystemURL(res.tempFilePath);
				setTimeout(() =>{
					uni.openDocument({
						filePath: filepathss,
						showMenu: false,
						success: function() {
							console.log("打开文档成功");
						},
						fail: function() {
							uni.showToast({
								title: '暂不支持此类型',
								duration: 2000,
								icon: "none",
							});
						}
					})
					uni.hideLoading();
				},1000);
			},
			fail: function(res) {
				console.log(res); //失败
				uni.hideLoading();
			}
		});
	}

}