# Uni-App模板Demo
#### 此项目适用于纯手机端的小程序、公众号、H5、APP； 不适用于PC、自适应等项目

#### 分支说明 
- **[master - 带Demo的分支](https://gitee.com/qq_admin_dev_team/uniapp-front/tree/master)** 
- **[uviewui - 不带DEMO的分支](https://gitee.com/qq_admin_dev_team/uniapp-front/tree/uviewui)**

#### 已集成插件及文档
- **[uviewui - UI组件(v1.7 - 2020-09)](https://www.uviewui.com/)**
- **[colorUI - UI组件(停止更新)](https://ext.dcloud.net.cn/plugin?id=471)**
- **[QS-SharePoster - Canvas海报生成](https://ext.dcloud.net.cn/plugin?id=471)**
- **[luch-request - 基于Promise的Request请求库](https://ext.dcloud.net.cn/plugin?id=392)**
- **[Parser富文本插件 - 富文本解析插件](https://ext.dcloud.net.cn/plugin?id=805)**
- **[uniapp插件市场](https://ext.dcloud.net.cn)**
- **[uniapp文档](https://uniapp.dcloud.io/component/README)**

#### 目录

~~~
|- components       UNIAPP插件目录
|- node_modules     Vue插件目录，小程序不建议使用
|- pages 			页面目录
|------ demo/form 			表单验证
|------ demo/request    	网络请求与文件上传
|------ demo/qrcode			分享二维码海报，通过canvas绘制保存
|------ demo/article 		富文本渲染
|------ demo/page 			PHP框架快速分页查询，适配java需要稍作改动 function.js		

|- static			静态资源目录
|------ images		图片目录
|------ js 			js封装目录 
|------------- api.js 		请求相关封装（带拦截器）
|------------- function.js  常用函数封装
|------------- md5.js
|- App.vue
|- config.js        配置文件
|- main.js          主入口文件
|- pages.json
|- uni.scss         全局CSS文件，建议合理提取重复的css
~~~


 

#### 网络请求 
1. GET请求
~~~
var _this=this;
this.$api.request({
	url:"/api/goods/search",
	//url也可以是完整链接，如：https://demo.com/api/xxxx
	method:"GET", //通过method指定请求类型, 默认POST
	data:{page:1,per_page:20,keyword:"便宜"},
	success:function(res){
		if(res.code==200){
			//数据渲染
		}else{
			_this.$toast(res.msg)
		}
	},
	fail:function(err){
		_this.$toast("请求出错");
		console.log("请求出错",err);
	}
})
~~~  
2. POST请求
~~~
var _this=this;
this.$api.request({
	url:"/api/goods/search",  
	data:{page:1,per_page:20,keyword:"便宜"},
	success:function(res){
		if(res.code==200){
			//数据渲染
		}else{
			_this.$toast(res.msg)
		}
	},
	fail:function(err){
		_this.$toast("请求出错");
		console.log("请求出错",err);
	}
})
~~~
3. 文件上传
~~~
var _this=this;
_this.$api.upload({
	url:"/api/attachment/upload",
	path:res.tempFilePaths[0],//本地文件路径
	name:"file",//文件的表单字段名称
	data:{dir:"images"},//自定义表单数据,
	success:function(res){
		if(res.code==200){
			
		}else{
			
		}
	},
	fail:function(err){
		
	} 
})
~~~

#### 表单验证
~~~ 
formSubmit:function(e){
	var datas=e.detail.value;
	//表单验证 ， 更多验证看function.js
	var msg=this.$func.validateParam([
		["姓名",datas.name,'require'], //require验证必填
		["姓名",datas.name,'length','1,20'],//length验证长度 
		['手机号',datas.mobile,'mobile'],//手机号验证
		['身份证',datas.idcard,'idcard'],//身份证验证
		['邮箱',datas.email,'email'],//邮箱验证
	]);
	console.log("表单数据",datas)
	if(msg!==true){
		this.$toast(msg);
		return false;
	}
	this.$showLoading("保存中"); //避免表单重复提交 
	
	var _this=this;
	this.$api.request({ //发送请求
		url:"/api/user/update",  
		data:datas,
		success:function(res){
			_this.$hideLoading();
			if(res.code==200){
				//保存成功
			}else{
				 
			}
		},
		fail:function(err){
			_this.$hideLoading();
			_this.$toast("请求出错");
			console.log("请求出错",err);
		}
	})
}
~~~

 
#### 其他JS封装

1. 浮点运算，避免出现js浮点bug
~~~
let res = this.$func.jia(1.9,1.82131);
let res = this.$func.jian(1.9,1.82131);
let res = this.$func.cheng(1.9,1.82131);
let res = this.$func.chu(1.9,1.82131);
~~~

2. 时间转换
~~~
var timestamp=this.$func.timestamp_now(); //当前时间戳

//0返回完整时间,1返回日期，2返回时分秒，3返回每个节点数组,4返回时分）

var arr=this.$func.datetime(timestamp,3);//获取格式化的年，月，日，时，分，秒，周 
console.log("格式化时间",arr);
console.log("今天是星期几",arr.weekday);

console.log('完整时间',this.$func.datetime(timestamp,0));
console.log('日期',this.$func.datetime(timestamp,1));
console.log('时间',this.$func.datetime(timestamp,2));
~~~

3. 快速缓存
~~~
this.$func.cache('userInfo',userObj);//可直接缓存json对象或字符串
var userInfo=this.$func.cache("userInfo");//读取缓存，优先进行一次json转换
console.log(userInfo.nickName);
~~~
4. 随机数、字符串
~~~
let rand = this.$func.rand(10000,999999); //随机整数
let randChar = this.$func.randChar(20); //随机字符串，无特殊符号
let randChar = this.$func.randChar(20,true); //随机字符串，含特殊符号
~~~
5. 带遮盖层的提示、加载中，可用于避免重复点击
~~~
this.$toast("验证码错误");
this.$toast("验证码错误",'success');//定义图标，默认none
this.$toast("验证码错误",null,5000);//定时关闭毫秒，默认1500 
this.$showLoading('上传中');
this.$hideLoading();
~~~

 

 