// vue.config.js
module.exports = {
  devServer: {
    proxy: {
      '/baseUrl': {
        target: 'https://web.zuodanke.com',
      }
    },
  }
}
